package com.rizvn.csvrecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Riz
 */
public class Group {
  String name = "group";
  List<Item> items = new ArrayList<Item>();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Item> getItems() {
    return items;
  }

  public void setItems(List<Item> items) {
    this.items = items;
  }

  public void addItem(Item item){
    items.add(item);
  }

  public String toXml(){
    StringBuilder sb = new StringBuilder();

    sb.append("<"+ name +">\n");

    for (Item item: items){
      sb.append(item.toXml() + "\n");
    }

    sb.append("</"+ name +">\n");

    return sb.toString();
  }
}

package com.rizvn.csvrecord;

/**
 * Created by Riz
 */
public class Item {
  String name = "item";
  String value = "";

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String toXml(){
    return "<"+ name+">" + value + "</"+ name+">";
  }
}

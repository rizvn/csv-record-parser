package com.rizvn.csvrecord;

import org.joox.JOOX;
import org.joox.Match;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Riz
 */
public class Parser {

  Match catalogue;

  List<Group> parse(String flowContent, String delimiter, String lineEndings){
    List<Group> groups = new ArrayList<Group>();

    String[] lines = flowContent.split(lineEndings);

    for(String line : lines){
      String[] items = line.split(delimiter);

      String groupName = items[0];

      Group group = new Group();
      group.setName(groupName);

      for(int i = 1; i < items.length; i++){
        Item item = new Item();
        item.setValue(items[i]);
        group.addItem(item);
      }
      groups.add(group);

      groups.forEach(grop -> nameItems(group));
    }
    return groups;
  }

  /**
   * Names group items using the xsd catalogue
   * @param group
   */
  public void nameItems(Group group){
    List<DefinedItem> definedItems = getDefinedItems(group.getName());

    int itemCount = definedItems.size() - group.getItems().size();

    //add additional items if missing
    for (int i =0; i<itemCount; i++){
      group.addItem(new Item());
    };

    for (int i =0; i< definedItems.size(); i++){
      group.getItems().get(i).setName(definedItems.get(i).getName());
    }
  }


  /**
   * load catalogue
   * @return
   */
  public Match getCatalogue(){
    if(catalogue == null){
      catalogue = JOOX.$(loadFromResource("/catalogue.xsd")).namespace("xs", "http://www.w3.org/2001/XMLSchema");
    }
    return catalogue;
  }

  /**
   * Item names in group
   * @param groupName
   * @return
   */
  public List<DefinedItem> getDefinedItems(String groupName){
    Match doc = getCatalogue();
    String groupType = doc.xpath("/xs:schema/xs:element[@name='"+ groupName +"']").attr("type");
    Match group      = doc.xpath("/xs:schema/xs:complexType[@name='"+ groupType+"']").first();
    Match elements   = JOOX.$(group.toString()).namespace("xs", "http://www.w3.org/2001/XMLSchema").xpath("//xs:element");

    List<DefinedItem> definedItems = new ArrayList<>();

    elements.forEach(el -> {
      DefinedItem definedItem = new DefinedItem();
      definedItem.setName(el.getAttribute("name"));
      definedItem.setType(el.getAttribute("type"));
      definedItems.add(definedItem);
    });

    return definedItems;
  }

  /**
   * Load resource as string
   * @param resourceName
   * @return
   */
  public String loadFromResource(String resourceName){
    try {
      return new Scanner(getClass().getResourceAsStream(resourceName), "UTF-8").useDelimiter("\\A").next();
    }
    catch (Exception ex){
      throw new IllegalStateException(ex);
    }
  }
}

package com.rizvn.csvrecord;

import org.joox.Match;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Riz
 */
public class ParserTest {


  @Test
  public void getCatalogue() throws Exception {
    Parser parser = new Parser();
    Match catalogue = parser.getCatalogue();
    Assert.assertNotNull(catalogue);
  }

  @Test
  public void getDefinedItems() throws Exception {
    Parser parser = new Parser();
    List<DefinedItem> elements = parser.getDefinedItems("121");
    Assert.assertFalse(elements.isEmpty());
  }

  @Test
  public void parseTest() throws Exception{
    Scanner scanner = new Scanner(new File("src/test/resources/aregi.uff"), "UTF-8");
    String xml = scanner.useDelimiter("\\A").next();
    scanner.close(); // Put this call in a finally block

    Parser parser = new Parser();
    List<Group> groups = parser.parse(xml, "\\|", "\n");

    StringBuilder stringBuilder = new StringBuilder();
    groups.forEach( group -> stringBuilder.append(group.toXml()));

    String flow = stringBuilder.toString();

    System.out.println(flow);

    Assert.assertFalse(groups.isEmpty());
  }

  @Test
  public void parseTestD010() throws Exception{
    Scanner scanner = new Scanner(new File("src/test/resources/D0010.uff"), "UTF-8");
    String xml = scanner.useDelimiter("\\A").next();
    scanner.close(); // Put this call in a finally block

    Parser parser = new Parser();
    List<Group> groups = parser.parse(xml, "\\|", "\n");

    StringBuilder stringBuilder = new StringBuilder();
    groups.forEach( group -> stringBuilder.append(group.toXml()));

    String flow = stringBuilder.toString();

    System.out.println(flow);

    Assert.assertFalse(groups.isEmpty());
  }
}